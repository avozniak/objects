module Validations

  def validate_number number
    until number =~ /\d/
      puts 'Invalid input type! Enter a number!'
      number = gets.chomp
    end
    number.to_i
  end

class Ticket

  include Validations

  attr_accessor :tax, :passenger, :session_id, :segment

  def initialize
    o = [('a'..'z'), ('A'..'Z'), (0..9)].map { |i| i.to_a }.flatten
    puts @session_id = (0...24).map { o[rand(o.length)] }.join
  end

  def passengers
    puts 'How many passengers?'
    @passenger = validate_number(gets.chomp)
  end

  def segments
    puts 'How many segments?'
    @segment = validate_number(gets.chomp)
  end

  def price
    @price = passengers*  segments * tax * 100
    puts " Price for #{ @passenger } passengers in #{ @segment } segments is #{ @price } $"
  end

end

class Avia < Ticket

  def tax
    @tax = 0.2
  end

end

class Railway < Ticket

  def tax
    @tax = 0.1
  end

end

class Bus < Ticket

  def tax
    @tax = 0.15
  end

end
end

order = Validations::Railway.new
puts order.price